package pl.qbix.primenumbers;

import java.util.TreeSet;
import java.util.stream.IntStream;

public class BasicPrimeCalculator implements PrimeCalculator {
    @Override
    public TreeSet<Integer> getPrimes(int maxValue) {
        TreeSet<Integer> primes = new TreeSet<>();
        IntStream.rangeClosed(1, maxValue)
                .filter(number -> isPrime(number))
                .forEach(number -> primes.add(number));
        return primes;
    }

    @Override
    public Integer getHighestPrime(int maxValue) {
        int highestPrime = 0;
        for (int i = 1 ; i<=maxValue ; i++){
            if(isPrime(i)){
                highestPrime = i;
            }
        }
        return highestPrime;
    }

    private boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
