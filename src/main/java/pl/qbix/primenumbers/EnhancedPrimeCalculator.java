package pl.qbix.primenumbers;

import com.sun.source.tree.Tree;

import java.util.TreeSet;
import java.util.stream.IntStream;

public class EnhancedPrimeCalculator implements PrimeCalculator {
    private TreeSet<Integer> primes = new TreeSet<>();

    @Override
    public TreeSet<Integer> getPrimes(int maxValue) {
        IntStream.rangeClosed(1, maxValue)
                .filter(this::isPrime)
                .forEach(number -> primes.add(number));
        return primes;
    }

    @Override
    public Integer getHighestPrime(int maxValue) {
        int highestPrime = 0;
        for (int i = 1; i <= maxValue; i++) {
            if (isPrime(i)) {
                primes.add(i);
                highestPrime = i;
            }
        }
        return highestPrime;
    }

    private boolean isPrime(int number) {
        for (int i : primes) {
            if (i == 1) continue;
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
