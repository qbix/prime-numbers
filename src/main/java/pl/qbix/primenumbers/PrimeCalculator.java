package pl.qbix.primenumbers;

import java.util.TreeSet;

public interface PrimeCalculator {
    TreeSet<Integer> getPrimes(int maxValue);
    Integer getHighestPrime(int maxValue);
}
