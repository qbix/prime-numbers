package pl.qbix.primenumbers

import spock.lang.Specification
import spock.lang.Subject

class NarrowingBasicPrimeCalculatorTest extends Specification {
    @Subject
    PrimeCalculator calculator = new NarrowingPrimeCaluclator()
    
    def "should return correct prime numbers"() {
        when:
            def primes = calculator.getPrimes(maxValue)
        
        then:
            primes == new TreeSet(expectedValues)
        where:
            maxValue | expectedValues
            1        | [1]
            2        | [1, 2]
            4        | [1, 2, 3]
            15       | [1, 2, 3, 5, 7, 11, 13]
    }
    
    
    def "should return highest found prime number"() {
        when:
            def prime = calculator.getHighestPrime(maxValue)
        
        then:
            prime == expectedValue
        where:
            maxValue | expectedValue
            1        | 1
            2        | 2
            4        | 3
            15       | 13
    }
    
}
