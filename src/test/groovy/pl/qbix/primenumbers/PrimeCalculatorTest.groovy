package pl.qbix.primenumbers

import spock.lang.Specification
import spock.lang.Unroll

class PrimeCalculatorTest extends Specification {
    @Unroll
    def "Compare times between calculators - #testCalculator"() {
        given:
            PrimeCalculator calculator = testCalculator;
            int maxValue = 100000
        
        when:
            long start = System.nanoTime()
            def primes = calculator.getPrimes(maxValue)
            long end = System.nanoTime()
            long time = end - start
            System.out.println("$calculator - time: $time")
        then:
            true
        where:
            testCalculator << [
                    new BasicPrimeCalculator(),
                    new NarrowingBasicPrimeCalculator(),
                    new EnhancedPrimeCalculator(),
                    new NarrowingPrimeCaluclator(),
                    new RootPrimeCalculator()
            ]
    }
    
    @Unroll
    def 'Compare times between calculators - single value - #testCalculator'(){
    
        given:
            PrimeCalculator calculator = testCalculator;
            int maxValue = 400000
    
        when:
            long start = System.nanoTime()
            def primes = calculator.getHighestPrime(maxValue)
            long end = System.nanoTime()
            long time = end - start
            System.out.println("single calculation: $calculator - time: $time - max Prime: $primes")
        then:
            true
        where:
            testCalculator << [
                    new BasicPrimeCalculator(),
                    new NarrowingBasicPrimeCalculator(),
                    new EnhancedPrimeCalculator(),
                    new NarrowingPrimeCaluclator(),
                    new RootPrimeCalculator()
            ]
    }
}
